using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.IO;

namespace SoccerHead
{
    class Controller
    {
        // Events & Delegates
        public delegate void CollisionHappened(CollisionObject2D collision);
        public static event CollisionHappened collisionHappenedEvent;

        Model model;
        World world;

        private void _on_Ball_body_entered(Godot.Object body)
        {
            GD.Print("TESTE BODY ENTERED");
            collisionHappenedEvent += model.OnCollision;
            
        }
        public void GAMEOVER()
        {

            // Tenta ler o ficheiro e escrever
            try
            {
                FileStream fs = System.IO.File.Open("C:\\Users\\Filipe\\Documents\\SoccerHead\\Content\\SoccerHead.txt", FileMode.Append);
                
                System.IO.File.ReadLines("C:\\Users\\Filipe\\Documents\\SoccerHead\\Content\\SoccerHead.txt");

                //Ler Score Guardado
                model.LerScoreGuardado(fs);

                //Guardar Record
                model.GuardarRecord(fs);

            }
            catch (System.IO.FileNotFoundException fnfe)
            {
                // Mostra mensagem de erro de ficheiro inexistente (World.cs - View)
                world.mensagemfilenaoexiste(fnfe);

                // Executa em caso de erro (Model.cs - Model)
                model.erroacao();

            }
            catch (System.IO.IOException ioe)
            {
                // Mostra mensagem de erro escrita no ficheiro (World.cs - View)
                world.mensagemfilenaoescreve(ioe);

                // Executa em caso de erro (Model.cs - Model)
                model.erroacao();
            }
        }
    }
}
