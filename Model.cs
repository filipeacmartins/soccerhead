using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SoccerHead
{
    public class Model
    {
        public delegate void CollisionHappened(CollisionObject2D collision);
        public static event CollisionHappened collisionHappenedEvent;
        public Label scoretext;
        private int score = 0;
        
        // Atualiza o Score do jogador
        public void UpdateLabel()
        {
            score += score;
            scoretext.Text = score.ToString();
        }

     
           IEnumerable<String> lines = null;
        // Lê o Score guardado no ficheiro .txt
        public void LerScoreGuardado(FileStream fs)
        {
            
                
        }
        
        // Carrega o Score para variável "score"
        public void CarregarScoreGuardado(int scorefile)
        {
            
        }

        // Escreve novo Record no ficheiro .txt
        public void GuardarRecord(FileStream fs)
        {
            StreamWriter stream = new StreamWriter(fs);
            stream.WriteLine(score); 
            stream.Close();
        }

        // Mostra erro e sai do jogo
        public void erroacao()
        {
            GD.Print("Sair do jogo");
        }
        // Mostra ecrã GAME OVER
        public void DISPLAYGAMEOVER()
        {
            //GetTree().ChangeScene("res://GAMEOVER.tscn");
        }

        // Ao colidir decide o que executar
        public void OnCollision(CollisionObject2D collision)
        {
            World world = new World();
            Controller controller = new Controller();
                      
            collisionHappenedEvent(collision);

            if(collision.Name=="Player") // Toque de cabeça = 1 ponto
            {
                GD.Print("Toque de cabeça");
                UpdateLabel();
            }
            else if (collision.Name=="Ground") // Bola toca no chão = GAME OVER
            {
                GD.Print("GAME OVER!!");
                DISPLAYGAMEOVER();      
                controller.GAMEOVER();
            }
 
        }
    }
}