using Godot;
using System.IO;
using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace SoccerHead
{
    public class World : Node
    {
        private void _on_Ball_body_entered(Godot.Object body)
        {
            GD.Print("TESTE BODY ENTERED");
        }
        public override void _Ready()
        {
            Connect("body_entered", this, nameof(_on_Ball_body_entered));            
        }
        public void mensagemfilenaoexiste(FileNotFoundException fnfe)
        {
            
            string message = "Score file doesn't exist";

            // Mostra mensagem
            GD.Print(message);
        }
        public void mensagemfilenaoescreve(IOException ioe)
        {
            string message = "Score file write error";

            // Mostra mensagem
            GD.Print(message);
        }
    }
}
